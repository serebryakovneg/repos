#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qmessagebox.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_radioButton_clicked(bool checked)
{
    QMessageBox::information(this, "Первый", "Вы успешно нажали первую кнопку!");
}

void MainWindow::on_radioButton_2_clicked(bool checked)
{
    QMessageBox::information(this, "Второй", "Вы успешно нажали вторую кнопку!");
}

void MainWindow::on_radioButton_3_clicked(bool checked)
{
    QMessageBox::information(this, "Третий", "Вы успешно нажали третью кнопку!");
}
